const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Donors = require('../models/donors');

//get all donors information
router.get('/', (req, res) => {
  console.log("Server - In Get");
  Donors.find({}, function(err, result) {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    });
  });

// add new donor
router.post('/', (req, res) => {
  console.log("Server - In Post");
  console.log(req.body);

  const donor = new Donors({
    _id: new mongoose.Types.ObjectId(),
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    mobileNumber : req.body.mobileNumber,
    address : req.body.address,
    lastTimeBloodDonated : new Date(req.body.lastTimeBloodDonated),
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  });
  
  donor.save()
    .then(result => {
      console.log(result);
    })
    .catch(err => console.log(err));

  res.status(201).json({
    message : "Handling POST request for blood-donor",
    createdDonor : donor
  });

});

//get donors information by id
router.get('/:donorId', (req,res) => {
  const id = req.params.donorId;
  console.log(id);
  Donors.findById(id)
  .exec()
  .then(doc => { 
    console.log("From Database: ", doc); 
    res.status(200).json(doc);
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({error : err});
  });
});

//update donor information
router.put("/:donorId", async (request, response) => {
  try {
      var donor = await Donors.findById(request.params.donorId).exec();
      console.log(donor);
      donor.set(request.body);
      console.log(donor);
      var result = await donor.save();
      response.send(result);
  } catch (error) {
      response.status(500).send(error);
  }
});

//patch donors information by id  
// body to patch : [ {"propName" : "firstName", "value" : "Chaitanya"} ]
router.patch('/:donorId', (req,res) => {
  const id = req.params.donorId;
  const updateOps = {};
  for(const ops of req.body){
    updateOps[ops.propName] = ops.value;
  }
  console.log(updateOps);
  Donors.update({ _id:id }, { $set : updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//delete donors information by id
router.delete('/:donorId', (req,res) => {
  const id = req.params.donorId;
  Donors.remove({_id : id})
  .exec()
  .then(result => {
    res.status(200).json(result);
  })
  .catch(err => {
    res.status(500).json({error : err});
  });
});
  
module.exports = router;