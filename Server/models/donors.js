const mongoose = require('mongoose');

const donorSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName: String,
    lastName: String,
    mobileNumber : Number,
    address : String,
    lastTimeBloodDonated : Date,
    email: String,
    username: String,
    password: String 
});

module.exports = mongoose.model('Donors', donorSchema);