const express = require('express'); //import express library
const app = express(); //initiate express app
const bodyParser = require('body-parser'); //parse POST body
const port = 5100;
const mongoose = require('mongoose');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json()); 

//allow CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization");
    if(req.method == 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','PUT, POST, PATCH, GET, DELETE');
        return res.status(200).json({});
    }
    next();
});

// import routes
const api = require('./routes/blood-donors');
app.use('/api/blood-donors', api);

// connect to mongo db
mongoose.connect('mongodb://localhost:27017/BloodDonarInfo', {useNewUrlParser: true, useUnifiedTopology: true}); 

app.get('/', function(req,res){
    res.send("Express Server connection is successful..")
});

//start listing to the 5100 port
app.listen(port, function(){
    console.log("Express Server running on http://localhost:"+ port)
});

//To run server > npm start