import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IDonorInfo } from '../Contracts/IDonorInfo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BloodDonorService {

  private _url = "http://localhost:5100/api/blood-donors";

  constructor(private _http: HttpClient) { }

  saveBloodDonorInfo(donor: IDonorInfo) : Observable<any>{
    console.log("In save method");
    console.log(JSON.stringify(donor));    
    return this._http.post(this._url, donor, {
      headers : new HttpHeaders({
      'Content-Type' : 'application/json'
      }) 
    });
  }

  getBloodDonors() : Observable<IDonorInfo[]> {
    console.log("In get method");
    return this._http.get<IDonorInfo[]>(this._url);
  }
}
