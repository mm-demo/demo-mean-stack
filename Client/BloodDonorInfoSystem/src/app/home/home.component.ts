import { Component, OnInit } from '@angular/core';
import { BloodDonorService } from '../Services/blood-donor.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  public donors = [];

  constructor(private _bloodDonorService: BloodDonorService) { }

  ngOnInit(): void {
    this._bloodDonorService.getBloodDonors()
    .subscribe(data =>  this.donors = data);

    console.log("Token");
    console.log(localStorage.getItem('token'));
  }

}
