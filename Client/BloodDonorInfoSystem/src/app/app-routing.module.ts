import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterDonorComponent } from './register-donor/register-donor.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { InvalidPageComponent } from './invalid-page/invalid-page.component';


const routes: Routes = [
  {path:'', component: HomeComponent, pathMatch:'full'},
  {path:'register', component: RegisterDonorComponent},
  {path:'login', component: LoginComponent},
  { path: '**', component: InvalidPageComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
