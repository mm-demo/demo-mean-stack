export interface IDonorInfo
{
    firstName: string,
    lastName: string,
    mobileNumber: string,
    address: string,
    lastTimeBloodDonated: string,
    email: string,
    username: string,
    password: string   
}