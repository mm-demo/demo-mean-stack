export class DonorInfo {
    constructor(
        public firstName: string,
        public lastName: string,
        public mobileNumber: string,
        public address: string,
        public lastTimeBloodDonated: string,
        public email: string,
        public username: string,
        public password: string       
    ) { }
}