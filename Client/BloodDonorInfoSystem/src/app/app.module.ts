import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterDonorComponent } from './register-donor/register-donor.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { BloodDonorService } from './Services/blood-donor.service';
import { HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { InvalidPageComponent } from './invalid-page/invalid-page.component'
import { AuthenticationService } from './Services/authentication.service';


@NgModule({
  declarations: [
    AppComponent,
    RegisterDonorComponent,
    LoginComponent,
    HomeComponent,
    InvalidPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [BloodDonorService, AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
