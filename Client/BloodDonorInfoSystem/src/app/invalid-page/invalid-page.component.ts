import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invalid-page',
  template: `
  <div class="mx-auto mt-5" style="width: 500px;">
  <h3>Page Not Found !</h3>
  </div>
  `,
  styles: [
  ]
})
export class InvalidPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
