import { Component, OnInit, ɵsetCurrentInjector } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DonorInfo } from '../Contracts/DonorInfo';
import { BloodDonorService } from '../Services/blood-donor.service';

@Component({
  selector: 'app-register-donor',
  templateUrl: './register-donor.component.html',
  styleUrls: ['./register-donor.component.css']
})
export class RegisterDonorComponent implements OnInit {

  public donors = [];

  donorInfo: DonorInfo;

  // Form Controls
  registerDonorForm;
  private firstName: FormControl;
  private lastName: FormControl;
  private mobileNumber: FormControl;
  private address: FormControl;
  private lastTimeBloodDonated: FormControl;
  private email: FormControl;
  private username: FormControl;
  private password: FormControl;

  constructor(private fb: FormBuilder, private _bloodDonorService: BloodDonorService) { }

  ngOnInit(): void {
    this.donorInfo = new DonorInfo('', '', '', '', '','','','');
    this.buildForm();
  }

  buildForm() {

    // setup the forms
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.mobileNumber = new FormControl('', Validators.required);
    this.address = new FormControl('', Validators.required);
    this.lastTimeBloodDonated = new FormControl('', Validators.required);
    this.email = new FormControl('', Validators.required);
    this.username = new FormControl('', Validators.required);
    this.password = new FormControl('', Validators.required);

    this.registerDonorForm = this.fb.group({
        firstName: this.firstName,
        lastName: this.lastName,
        mobileNumber: this.mobileNumber,
        address: this.address,
        lastTimeBloodDonated: this.lastTimeBloodDonated,
        email: this.email,
        username: this.username,
        password: this.password,
    });
}

doRegisterDonor() {
  console.log(this.registerDonorForm);
  if (this.registerDonorForm.valid) {
    console.log("form is valid");
    let info = this.registerDonorForm.value;
    this.donorInfo = new DonorInfo(info.firstName, info.lastName, info.mobileNumber, info.address, info.lastTimeBloodDonated,info.email, info.username,info.password);
    this._bloodDonorService.saveBloodDonorInfo(this.donorInfo)
    .subscribe(data => {
      console.log("Donor Registered Successfully");
      console.log(data);
    });
  }
}

GetInfo(){
  this._bloodDonorService.getBloodDonors()
  .subscribe(data =>  this.donors = data);
  console.log(this.donors);
  alert(JSON.stringify(this.donors));
}

}
